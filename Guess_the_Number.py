from random import randint

if __name__ == '__main__':
	
	while True:
		counter = 0

		print("\nWelcome to Guess the Number!"
	    	  "\nSelect the range of numbers you want to choose from.")

		while True:
			try:
				low = int(raw_input("Lowest value: "))
				high = int(raw_input("Highest value: "))
				while (high < low):
					print ("Highest value less than lowest value(%d)!" % low)
					high = int(raw_input("Enter a new highest value: "))
				break
			except ValueError:
				print ("Oops! That was not a valid number. Try again...")

		rand_num = randint(low, high)

		print("\nRandom Number Generated!")

		while True:
			try:
				user_num = int(raw_input("Guess the Number!: "))
				counter += 1
				if user_num == rand_num:
					print("\nYou guessed correct!!! It took %d guess(es)." % counter)
					break
				elif user_num < rand_num:
					print("Guess higher!")
				else:
					print("Guess lower!")
			except ValueError:
				print ("Oops! That was not a valid number. Try again...")

		user_decision = raw_input("Would you like to play again? (y/n): ").lower()

		if user_decision.startswith('y'):
			continue
		else:
			break